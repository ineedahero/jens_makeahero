﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Heroes;
using Microsoft.Data.SqlClient;

namespace CreateAHero
{
    public class HandleFormLogic
    {
        public bool primeTypeFirstTimeChecked = true;

        public string[] subtypesWarrior = { "AxMan", "Knight" };
        public string[] subtypesWizard = { "Mage", "Necromancer" };
        public string[] subtypesThief = { "Burglar", "Robber" };

        public Hero mainCharacter;

        public static Image getBackgroundImage()
        {
            // Tries to get and set the background image for the form
            Image fetchedImg = new Bitmap(400, 400);
            var request = WebRequest.Create("https://www.freod.com/application/files/2615/1244/8434/cartoonVillage.jpg");
            try
            {
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    fetchedImg = Bitmap.FromStream(stream);
                }
            }
            catch (Exception e) { Console.WriteLine("An error occurred while getting the background image."); }

            return fetchedImg;
        }

        public Hero makeAHeroClass(string classChosen, string name, int speed)
        {
            switch (classChosen)
            {
                case "AxMan":
                    mainCharacter = new AxMan(name, speed);
                    break;
                case "Knight":
                    mainCharacter = new Knigth(name, speed);
                    break;
                case "Mage":
                    mainCharacter = new Mage(name, speed);
                    break;
                case "Necromancer":
                    mainCharacter = new Necromancer(name, speed);
                    break;
                case "Burglar":
                    mainCharacter = new Burglar(name);
                    break;
                case "Robber":
                    mainCharacter = new Robber(name);
                    break;
            }

            return mainCharacter;
        }

        public void sendHeroInfoToDB()
        {
            string[] heroAttributes = { "Name", "Hp", "Mana", "AtkDmg", "Armor", "Distance" };

            // Build connection string
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = @"PC7276\SQLEXPRESS";
            builder.InitialCatalog = "heroesDB";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "INSERT INTO dbo.INeedMyHero(";

                    // Set up attributes that should be added to the DB
                    int attributeNum = 0;
                    foreach (var attribute in heroAttributes)
                    {
                        if (attributeNum < heroAttributes.Length - 1) { sql = sql + $"{attribute},"; }
                        else { sql = sql + $"{attribute}) VALUES("; }
                        attributeNum += 1;
                    }

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;

                        // Insert the actual values of the hero into the SQL statement
                        sql += $"\'{mainCharacter.Name}\'";
                        sql += $",{mainCharacter.Hp}";
                        sql += $",{mainCharacter.Mana}";
                        sql += $",{mainCharacter.AttackDamage}";
                        sql += $",{mainCharacter.ArmorRating}";
                        sql += $",{mainCharacter.Distance}";
                        sql += ")";

                        Console.WriteLine("After adding values: " + sql);
                        command.CommandText = sql;
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Hero getHeroFromDB(string nameOfHero)
        {
            Hero heroFromDB = new Hero("");

            // Build connection string
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = @"PC7276\SQLEXPRESS";
            builder.InitialCatalog = "heroesDB";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    string sql = $"SELECT * FROM dbo.INeedMyHero WHERE Name=\'{nameOfHero}\'";

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = sql;
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine(reader.GetString(1));
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return heroFromDB;
        }
    }
}
