﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Heroes;
using Newtonsoft.Json;
using Microsoft.Data.SqlClient;

namespace CreateAHero
{
    public partial class CreateYourHero : Form
    {
        public bool primeTypeFirstTimeChecked = true;
        public string[] subtypesWarrior = { "AxMan", "Knight" };
        public string[] subtypesWizard = { "Mage", "Necromancer" };
        public string[] subtypesThief = { "Burglar", "Robber" };

        public GroupBox secondaryTypeBox;
        public RadioButton radBtnFirstSecondary;
        public RadioButton radBtnSecondSecondary;

        Hero mainCharacter;
        HandleFormLogic handleLogic = new HandleFormLogic();


        public CreateYourHero()
        {
            InitializeComponent();

            // Tries to get and set the background image for the form
            this.BackgroundImage = HandleFormLogic.getBackgroundImage();
        }

        public void createHero_Click(object sender, EventArgs e)
        {

            if (warriorRButton.Checked || wizardRButton.Checked || thiefRButton.Checked)
            {
                if (radBtnFirstSecondary.Checked || radBtnSecondSecondary.Checked)
                {
                    if (nameBox.Text == "") { nameBox.Text = "Red Riding Hood"; }
                    
                    //Save!
                    string whichClass = radBtnFirstSecondary.Checked ? radBtnFirstSecondary.Text : radBtnSecondSecondary.Text;
                    aHeroIsNotBornButMade(whichClass);
                }
            }
        }


        public void checkedPrimeType(object sender, EventArgs e) 
        {
            if (primeTypeFirstTimeChecked)
            {
                createRButtonsForSubTypes();
            }

            //When a new prime type is selected, remove checking of subtypes from previous time
            this.radBtnFirstSecondary.Checked = false;
            this.radBtnSecondSecondary.Checked = false;

            RadioButton radBtnClicked = (RadioButton) sender;
            setOptionNamesForSubtypeRButtons(radBtnClicked.Name);

        }

        public void createRButtonsForSubTypes()
        {
            this.secondaryTypeBox = new GroupBox();
            this.secondaryTypeBox.Text = "Secondary type";
            this.secondaryTypeBox.Location = primaryTypeBox.Location;
            this.secondaryTypeBox.Left += 125;
            this.Controls.Add(this.secondaryTypeBox);

            this.radBtnFirstSecondary = new RadioButton();
            this.radBtnSecondSecondary = new RadioButton();
            this.radBtnFirstSecondary.Location = new Point(20, 20);
            this.radBtnSecondSecondary.Location = new Point(20, 45);
            secondaryTypeBox.Controls.Add(this.radBtnFirstSecondary);
            secondaryTypeBox.Controls.Add(this.radBtnSecondSecondary);
            primeTypeFirstTimeChecked = false;
        }

        public void setOptionNamesForSubtypeRButtons(string nameOfRButtonThatSentEvent)
        {
            switch (nameOfRButtonThatSentEvent)
            {
                case "warriorRButton":
                    this.radBtnFirstSecondary.Text = subtypesWarrior[0];
                    this.radBtnSecondSecondary.Text = subtypesWarrior[1];
                    speedLabel.Visible = true;
                    speedChooser.Visible = true;
                    break;
                case "wizardRButton":
                    this.radBtnFirstSecondary.Text = subtypesWizard[0];
                    this.radBtnSecondSecondary.Text = subtypesWizard[1];
                    speedLabel.Visible = true;
                    speedChooser.Visible = true;
                    break;
                case "thiefRButton":
                    this.radBtnFirstSecondary.Text = subtypesThief[0];
                    this.radBtnSecondSecondary.Text = subtypesThief[1];
                    speedLabel.Visible = false;
                    speedChooser.Visible = false;
                    break;
                default:
                    this.radBtnFirstSecondary.Text = "First subtype";
                    this.radBtnSecondSecondary.Text = "Second subtype";
                    break;

            }
        }

        public void aHeroIsNotBornButMade(string classChosen)
        {
            string name = nameBox.Text;
            int speed = Decimal.ToInt32(speedChooser.Value);
            mainCharacter = handleLogic.makeAHeroClass(classChosen, name, speed);

            handleLogic.sendHeroInfoToDB();
            HeroSummary summaryForHero = new HeroSummary(mainCharacter.Name, 
                                                            mainCharacter.Hp, 
                                                            mainCharacter.Mana, 
                                                            mainCharacter.ArmorRating, 
                                                            mainCharacter.Distance, 
                                                            mainCharacter.AttackDamage);
            summaryForHero.ShowDialog();
            this.Close();
        }
    }
}
