﻿namespace CreateAHero
{
    partial class CreateYourHero
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createHero = new System.Windows.Forms.Button();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.fillIfCreatedLabel = new System.Windows.Forms.Label();
            this.warriorRButton = new System.Windows.Forms.RadioButton();
            this.wizardRButton = new System.Windows.Forms.RadioButton();
            this.thiefRButton = new System.Windows.Forms.RadioButton();
            this.nameLabel = new System.Windows.Forms.Label();
            this.primaryTypeBox = new System.Windows.Forms.GroupBox();
            this.speedChooser = new System.Windows.Forms.NumericUpDown();
            this.speedLabel = new System.Windows.Forms.Label();
            this.primaryTypeBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedChooser)).BeginInit();
            this.SuspendLayout();
            // 
            // createHero
            // 
            this.createHero.Font = new System.Drawing.Font("Algerian", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createHero.Location = new System.Drawing.Point(450, 315);
            this.createHero.Name = "createHero";
            this.createHero.Size = new System.Drawing.Size(169, 71);
            this.createHero.TabIndex = 0;
            this.createHero.Text = "Create";
            this.createHero.UseVisualStyleBackColor = true;
            this.createHero.Click += new System.EventHandler(this.createHero_Click);
            // 
            // nameBox
            // 
            this.nameBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameBox.Location = new System.Drawing.Point(519, 173);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(232, 31);
            this.nameBox.TabIndex = 1;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Castellar", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(97, 24);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(585, 58);
            this.titleLabel.TabIndex = 5;
            this.titleLabel.Text = "Create your hero!";
            // 
            // fillIfCreatedLabel
            // 
            this.fillIfCreatedLabel.AutoSize = true;
            this.fillIfCreatedLabel.Location = new System.Drawing.Point(291, 117);
            this.fillIfCreatedLabel.Name = "fillIfCreatedLabel";
            this.fillIfCreatedLabel.Size = new System.Drawing.Size(0, 13);
            this.fillIfCreatedLabel.TabIndex = 6;
            // 
            // warriorRButton
            // 
            this.warriorRButton.AutoSize = true;
            this.warriorRButton.Location = new System.Drawing.Point(6, 21);
            this.warriorRButton.Name = "warriorRButton";
            this.warriorRButton.Size = new System.Drawing.Size(77, 20);
            this.warriorRButton.TabIndex = 7;
            this.warriorRButton.TabStop = true;
            this.warriorRButton.Text = "Warrior";
            this.warriorRButton.UseVisualStyleBackColor = true;
            this.warriorRButton.CheckedChanged += new System.EventHandler(this.checkedPrimeType);
            // 
            // wizardRButton
            // 
            this.wizardRButton.AutoSize = true;
            this.wizardRButton.Location = new System.Drawing.Point(6, 39);
            this.wizardRButton.Name = "wizardRButton";
            this.wizardRButton.Size = new System.Drawing.Size(74, 20);
            this.wizardRButton.TabIndex = 8;
            this.wizardRButton.TabStop = true;
            this.wizardRButton.Text = "Wizard";
            this.wizardRButton.UseVisualStyleBackColor = true;
            this.wizardRButton.CheckedChanged += new System.EventHandler(this.checkedPrimeType);
            // 
            // thiefRButton
            // 
            this.thiefRButton.AutoSize = true;
            this.thiefRButton.Location = new System.Drawing.Point(6, 56);
            this.thiefRButton.Name = "thiefRButton";
            this.thiefRButton.Size = new System.Drawing.Size(61, 20);
            this.thiefRButton.TabIndex = 9;
            this.thiefRButton.TabStop = true;
            this.thiefRButton.Text = "Thief";
            this.thiefRButton.UseVisualStyleBackColor = true;
            this.thiefRButton.CheckedChanged += new System.EventHandler(this.checkedPrimeType);
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(437, 172);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(76, 34);
            this.nameLabel.TabIndex = 10;
            this.nameLabel.Text = "Name";
            // 
            // primaryTypeBox
            // 
            this.primaryTypeBox.Controls.Add(this.warriorRButton);
            this.primaryTypeBox.Controls.Add(this.wizardRButton);
            this.primaryTypeBox.Controls.Add(this.thiefRButton);
            this.primaryTypeBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.primaryTypeBox.Location = new System.Drawing.Point(22, 184);
            this.primaryTypeBox.Name = "primaryTypeBox";
            this.primaryTypeBox.Size = new System.Drawing.Size(112, 83);
            this.primaryTypeBox.TabIndex = 12;
            this.primaryTypeBox.TabStop = false;
            this.primaryTypeBox.Text = "Primary type";
            // 
            // speedChooser
            // 
            this.speedChooser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speedChooser.Location = new System.Drawing.Point(520, 220);
            this.speedChooser.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.speedChooser.Name = "speedChooser";
            this.speedChooser.Size = new System.Drawing.Size(120, 26);
            this.speedChooser.TabIndex = 13;
            this.speedChooser.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.speedChooser.Visible = false;
            // 
            // speedLabel
            // 
            this.speedLabel.AutoSize = true;
            this.speedLabel.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speedLabel.Location = new System.Drawing.Point(437, 217);
            this.speedLabel.Name = "speedLabel";
            this.speedLabel.Size = new System.Drawing.Size(73, 34);
            this.speedLabel.TabIndex = 14;
            this.speedLabel.Text = "Speed";
            this.speedLabel.Visible = false;
            // 
            // CreateYourHero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSalmon;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.speedLabel);
            this.Controls.Add(this.speedChooser);
            this.Controls.Add(this.primaryTypeBox);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.fillIfCreatedLabel);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.createHero);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimumSize = new System.Drawing.Size(800, 400);
            this.Name = "CreateYourHero";
            this.Opacity = 0.9D;
            this.Text = "Create A Hero!";
            this.primaryTypeBox.ResumeLayout(false);
            this.primaryTypeBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedChooser)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button createHero;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label fillIfCreatedLabel;
        private System.Windows.Forms.RadioButton warriorRButton;
        private System.Windows.Forms.RadioButton wizardRButton;
        private System.Windows.Forms.RadioButton thiefRButton;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.GroupBox primaryTypeBox;
        private System.Windows.Forms.NumericUpDown speedChooser;
        private System.Windows.Forms.Label speedLabel;
    }
}

