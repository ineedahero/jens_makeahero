﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CreateAHero
{
    public partial class HeroSummary : Form
    {


        public HeroSummary(string name, int hp, int mana, int armorRating, int speed, int attackDmg)
        {
            InitializeComponent();
            if (name.Length > 8)
            {
                fillName.Left -= 60;
            }
            fillName.Text = name;
            fillHP.Text = hp.ToString();
            fillMana.Text = mana.ToString();
            fillArmor.Text = armorRating.ToString();
            fillSpeed.Text = speed.ToString();
            fillDamage.Text = attackDmg.ToString();

            // Tries to get and set the background image for the form
            var request = WebRequest.Create("https://wearvr-static.global.ssl.fastly.net/lists/curateds/cover_images/000/000/101/large/Superhero-VR.jpg?1517309823");
            try
            {
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    this.BackgroundImage = Bitmap.FromStream(stream);
                }
            }
            catch (Exception e) { Console.WriteLine("An error occurred while getting the background image."); }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
