﻿namespace CreateAHero
{
    partial class HeroSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        public System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.hpLabel = new System.Windows.Forms.Label();
            this.armorLabel = new System.Windows.Forms.Label();
            this.manaLabel = new System.Windows.Forms.Label();
            this.damageLabel = new System.Windows.Forms.Label();
            this.speedLabel = new System.Windows.Forms.Label();
            this.fillSpeed = new System.Windows.Forms.Label();
            this.fillDamage = new System.Windows.Forms.Label();
            this.fillMana = new System.Windows.Forms.Label();
            this.fillArmor = new System.Windows.Forms.Label();
            this.fillHP = new System.Windows.Forms.Label();
            this.fillName = new System.Windows.Forms.Label();
            this.closeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // hpLabel
            // 
            this.hpLabel.AutoSize = true;
            this.hpLabel.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hpLabel.Location = new System.Drawing.Point(142, 133);
            this.hpLabel.Name = "hpLabel";
            this.hpLabel.Size = new System.Drawing.Size(47, 34);
            this.hpLabel.TabIndex = 1;
            this.hpLabel.Text = "HP";
            // 
            // armorLabel
            // 
            this.armorLabel.AutoSize = true;
            this.armorLabel.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.armorLabel.Location = new System.Drawing.Point(142, 167);
            this.armorLabel.Name = "armorLabel";
            this.armorLabel.Size = new System.Drawing.Size(141, 34);
            this.armorLabel.TabIndex = 2;
            this.armorLabel.Text = "Armor rating";
            // 
            // manaLabel
            // 
            this.manaLabel.AutoSize = true;
            this.manaLabel.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manaLabel.Location = new System.Drawing.Point(142, 235);
            this.manaLabel.Name = "manaLabel";
            this.manaLabel.Size = new System.Drawing.Size(74, 34);
            this.manaLabel.TabIndex = 3;
            this.manaLabel.Text = "Mana";
            // 
            // damageLabel
            // 
            this.damageLabel.AutoSize = true;
            this.damageLabel.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.damageLabel.Location = new System.Drawing.Point(142, 201);
            this.damageLabel.Name = "damageLabel";
            this.damageLabel.Size = new System.Drawing.Size(94, 34);
            this.damageLabel.TabIndex = 4;
            this.damageLabel.Text = "Damage";
            // 
            // speedLabel
            // 
            this.speedLabel.AutoSize = true;
            this.speedLabel.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speedLabel.Location = new System.Drawing.Point(142, 272);
            this.speedLabel.Name = "speedLabel";
            this.speedLabel.Size = new System.Drawing.Size(68, 34);
            this.speedLabel.TabIndex = 5;
            this.speedLabel.Text = "Speed";
            // 
            // fillSpeed
            // 
            this.fillSpeed.AutoSize = true;
            this.fillSpeed.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fillSpeed.Location = new System.Drawing.Point(290, 272);
            this.fillSpeed.Name = "fillSpeed";
            this.fillSpeed.Size = new System.Drawing.Size(68, 34);
            this.fillSpeed.TabIndex = 11;
            this.fillSpeed.Text = "Speed";
            // 
            // fillDamage
            // 
            this.fillDamage.AutoSize = true;
            this.fillDamage.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fillDamage.Location = new System.Drawing.Point(290, 201);
            this.fillDamage.Name = "fillDamage";
            this.fillDamage.Size = new System.Drawing.Size(94, 34);
            this.fillDamage.TabIndex = 10;
            this.fillDamage.Text = "Damage";
            // 
            // fillMana
            // 
            this.fillMana.AutoSize = true;
            this.fillMana.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fillMana.Location = new System.Drawing.Point(290, 235);
            this.fillMana.Name = "fillMana";
            this.fillMana.Size = new System.Drawing.Size(74, 34);
            this.fillMana.TabIndex = 9;
            this.fillMana.Text = "Mana";
            // 
            // fillArmor
            // 
            this.fillArmor.AutoSize = true;
            this.fillArmor.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fillArmor.Location = new System.Drawing.Point(290, 167);
            this.fillArmor.Name = "fillArmor";
            this.fillArmor.Size = new System.Drawing.Size(141, 34);
            this.fillArmor.TabIndex = 8;
            this.fillArmor.Text = "Armor rating";
            // 
            // fillHP
            // 
            this.fillHP.AutoSize = true;
            this.fillHP.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fillHP.Location = new System.Drawing.Point(290, 133);
            this.fillHP.Name = "fillHP";
            this.fillHP.Size = new System.Drawing.Size(47, 34);
            this.fillHP.TabIndex = 7;
            this.fillHP.Text = "HP";
            // 
            // fillName
            // 
            this.fillName.AutoSize = true;
            this.fillName.Font = new System.Drawing.Font("Viner Hand ITC", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fillName.Location = new System.Drawing.Point(206, 47);
            this.fillName.Name = "fillName";
            this.fillName.Size = new System.Drawing.Size(95, 44);
            this.fillName.TabIndex = 6;
            this.fillName.Text = "Name";
            // 
            // closeButton
            // 
            this.closeButton.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.Location = new System.Drawing.Point(102, 354);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(292, 47);
            this.closeButton.TabIndex = 12;
            this.closeButton.Text = "Finished";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // HeroSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 423);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.fillSpeed);
            this.Controls.Add(this.fillDamage);
            this.Controls.Add(this.fillMana);
            this.Controls.Add(this.fillArmor);
            this.Controls.Add(this.fillHP);
            this.Controls.Add(this.fillName);
            this.Controls.Add(this.speedLabel);
            this.Controls.Add(this.damageLabel);
            this.Controls.Add(this.manaLabel);
            this.Controls.Add(this.armorLabel);
            this.Controls.Add(this.hpLabel);
            this.Name = "HeroSummary";
            this.Text = "Hero Summary";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Label hpLabel;
        public System.Windows.Forms.Label armorLabel;
        public System.Windows.Forms.Label manaLabel;
        public System.Windows.Forms.Label damageLabel;
        public System.Windows.Forms.Label speedLabel;
        public System.Windows.Forms.Label fillSpeed;
        public System.Windows.Forms.Label fillDamage;
        public System.Windows.Forms.Label fillMana;
        public System.Windows.Forms.Label fillArmor;
        public System.Windows.Forms.Label fillHP;
        public System.Windows.Forms.Label fillName;
        public System.Windows.Forms.Button closeButton;
    }
}