Run the CreateYourHero Application file to run the program (only works with connection string "PC7276\SQLEXPRESS", with a database with the name heroesDB and table dbo.INeedMyHero)

Classes
* CreateYourHero    -> Winform that gets input on hero type, name
* HandleFormLogic   -> Handles the creation of the new hero, saving to DB etc
* HeroSummary       -> Shows a summary of the new hero when done


**Relevant pictures**

![](relevant_pictures/front_page.png)
![](relevant_pictures/hero_summary.png)
![](relevant_pictures/results_from_db.png)
